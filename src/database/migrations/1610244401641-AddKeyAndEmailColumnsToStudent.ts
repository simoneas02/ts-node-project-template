import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddKeyAndEmailColumnsToStudent1610244401641
  implements MigrationInterface {
  name = 'AddKeyAndEmailColumnsToStudent1610244401641';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "student" ADD "key" integer`);

    await queryRunner.query(
      `ALTER TABLE "student" ADD "email" character varying`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "student" DROP COLUMN "email"`);

    await queryRunner.query(`ALTER TABLE "student" DROP COLUMN "key"`);
  }
}
