import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateDigitalClassAndLessonRelationship1610227440171
  implements MigrationInterface {
  name = 'CreateDigitalClassAndLessonRelationship1610227440171';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "lesson" ADD "digitalClassId" uuid`);

    await queryRunner.query(
      `ALTER TABLE "lesson" ADD CONSTRAINT "FK_8ed875b271a4eccd9fccbf2db35" FOREIGN KEY ("digitalClassId") REFERENCES "digital_class"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "lesson" DROP CONSTRAINT "FK_8ed875b271a4eccd9fccbf2db35"`,
    );

    await queryRunner.query(
      `ALTER TABLE "lesson" DROP COLUMN "digitalClassId"`,
    );
  }
}
