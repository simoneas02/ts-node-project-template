import { MigrationInterface, QueryRunner } from 'typeorm';

export class RenameClassTable1610214217303 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.renameTable('class', 'digital_class');
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.renameTable('digital_class', 'class');
  }
}
