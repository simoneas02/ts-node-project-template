import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateDigitalClassAndStudentRelationship1610238726339
  implements MigrationInterface {
  name = 'CreateDigitalClassAndStudentRelationship1610238726339';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "student_digital_classes_digital_class" ("studentId" uuid NOT NULL, "digitalClassId" uuid NOT NULL, CONSTRAINT "PK_b561fc01ccf529be11a499b7434" PRIMARY KEY ("studentId", "digitalClassId"))`,
    );

    await queryRunner.query(
      `CREATE INDEX "IDX_2d554b0d2d32b2dac0d37f5c29" ON "student_digital_classes_digital_class" ("studentId") `,
    );

    await queryRunner.query(
      `CREATE INDEX "IDX_75d7c16f164d88d7f930d3ca80" ON "student_digital_classes_digital_class" ("digitalClassId") `,
    );

    await queryRunner.query(
      `ALTER TABLE "student_digital_classes_digital_class" ADD CONSTRAINT "FK_2d554b0d2d32b2dac0d37f5c293" FOREIGN KEY ("studentId") REFERENCES "student"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );

    await queryRunner.query(
      `ALTER TABLE "student_digital_classes_digital_class" ADD CONSTRAINT "FK_75d7c16f164d88d7f930d3ca801" FOREIGN KEY ("digitalClassId") REFERENCES "digital_class"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "student_digital_classes_digital_class" DROP CONSTRAINT "FK_75d7c16f164d88d7f930d3ca801"`,
    );

    await queryRunner.query(
      `ALTER TABLE "student_digital_classes_digital_class" DROP CONSTRAINT "FK_2d554b0d2d32b2dac0d37f5c293"`,
    );

    await queryRunner.query(`DROP INDEX "IDX_75d7c16f164d88d7f930d3ca80"`);

    await queryRunner.query(`DROP INDEX "IDX_2d554b0d2d32b2dac0d37f5c29"`);

    await queryRunner.query(
      `DROP TABLE "student_digital_classes_digital_class"`,
    );
  }
}
