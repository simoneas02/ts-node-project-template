import { EntityRepository, Repository } from 'typeorm';
import DigitalClass from '../models/DigitalClass';

@EntityRepository()
export default class ClassRepository extends Repository<DigitalClass> {
  public async findByName(name: string): Promise<DigitalClass[]> {
    return this.find({
      where: { name },
    });
  }
}
