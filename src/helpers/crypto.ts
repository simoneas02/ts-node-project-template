import dotenv from 'dotenv';
import { appConfig } from '../config/appConfig';
import { EncryptionTransformer } from 'typeorm-encrypted';

dotenv.config();

export const Crypto = new EncryptionTransformer({
  key: appConfig().cryptoKey,
  algorithm: 'aes-256-cbc',
  ivLength: 16,
  iv: appConfig().cryptoIV,
});
