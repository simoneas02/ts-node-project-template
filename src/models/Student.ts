import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

import { IsEmail, Max, MaxLength, Min, MinLength } from 'class-validator';

import DigitalClass from './DigitalClass';
import { Crypto } from '../helpers/crypto';

@Entity()
export class Student {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToMany(type => DigitalClass, { eager: true })
  @JoinTable()
  digitalClasses: DigitalClass[];

  @Column({
    transformer: {
      from: (value: string) => value.toLowerCase(),
      to: (value: string) => value.toUpperCase(),
    },
  })
  @MaxLength(50, {
    message: 'Student name must be shorter than or equal to 50 characters',
  })
  @MinLength(2, {
    message: 'Student name must be longer than or equal to 2 characters',
  })
  name: string;

  @Column()
  @Max(9999)
  @Min(100)
  key: number;

  @Column({
    transformer: Crypto,
  })
  @IsEmail()
  email: string;

  @CreateDateColumn({ name: 'created_At' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_At' })
  updatedAt: Date;
}
