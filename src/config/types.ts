import { merge } from 'lodash';
import { DeepPartial } from 'typeorm';
import { ExternalConfig } from './externalConfig';

export interface RuntimeEnvironmentConfig {
  readonly applicationName: string;
  readonly port: string;
  readonly cryptoKey: string;
  readonly cryptoIV: string;

  readonly database: {
    readonly username: string;
    readonly password: string;
    readonly port: string;
    readonly host: string;
    readonly database: string;
    readonly ssl: boolean;
  };
}

export const extend = (existingConfig: Env, newConfig: Env): Env => (
  ...args
): {} => merge(existingConfig(...args), newConfig(...args));

export type Env = (
  externalConfig: ExternalConfig,
) => DeepPartial<RuntimeEnvironmentConfig>;
