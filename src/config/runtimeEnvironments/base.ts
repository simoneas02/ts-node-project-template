import { externalConfig } from '../externalConfig';
import { Env } from '../types';

export const base: Env = () => ({
  applicationName: 'ts-node-template',
  cryptoKey: externalConfig.CRYPTO_KEY,
  cryptoIV: externalConfig.CRYPTO_IV,
});
