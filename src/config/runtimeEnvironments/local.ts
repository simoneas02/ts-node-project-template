import { Env, extend } from '../types';

import { base } from './base';

export const local: Env = extend(base, externalConfig => ({
  database: {
    username: externalConfig.TYPEORM_USERNAME,
    password: externalConfig.TYPEORM_PASSWORD,
    port: externalConfig.TYPEORM_PORT,
    host: externalConfig.TYPEORM_HOST,
    database: externalConfig.TYPEORM_DATABASE,
    ssl: false,
  },
}));
