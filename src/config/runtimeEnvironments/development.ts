import { Env, extend } from '../types';
import { base } from './base';

export const development: Env = extend(base, externalConfig => ({
  port: externalConfig.PORT,
  database: {
    username: externalConfig.TYPEORM_USERNAME,
    password: externalConfig.TYPEORM_PASSWORD,
    port: externalConfig.TYPEORM_PORT,
    host: externalConfig.DATABASE_SERVER,
    database: 'pretalab_dev',
    ssl: true,
  },
}));
