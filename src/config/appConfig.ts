import { externalConfig } from './externalConfig';
import { Env, RuntimeEnvironmentConfig } from './types';
import { local } from './runtimeEnvironments/local';
import { localcloud } from './runtimeEnvironments/localcloud';
import { development } from './runtimeEnvironments/development';
import { qa } from './runtimeEnvironments/qa';
import { production } from './runtimeEnvironments/production';

const allRuntimeEnvironments: { [name: string]: Env } = {
  local,
  development,
  qa,
  production,
  localcloud,
};

const DEFAULT_RUNTIME_ENVIRONMENT = 'local';

export type AppConfig = RuntimeEnvironmentConfig & {
  runtimeEnvironment: string;
};

export const appConfig = (): AppConfig => {
  const runtimeEnvironment =
    externalConfig.ENVIRONMENT || DEFAULT_RUNTIME_ENVIRONMENT;

  const runtimeEnvironmentConfig = allRuntimeEnvironments[runtimeEnvironment];

  return {
    ...(runtimeEnvironmentConfig(externalConfig) as RuntimeEnvironmentConfig),
    runtimeEnvironment,
  };
};
