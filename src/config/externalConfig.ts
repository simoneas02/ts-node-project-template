const NODE_ENVIRONMENT_VARIABLES = [
  'ENVIRONMENT',
  'PORT',
  'TYPEORM_DATABASE',
  'TYPEORM_HOST',
  'TYPEORM_PASSWORD',
  'TYPEORM_PORT',
  'TYPEORM_USERNAME',
  'DATABASE_SERVER',
  'CRYPTO_KEY',
  'CRYPTO_IV',
] as const;

export type ExternalConfig = Partial<
  { [s in typeof NODE_ENVIRONMENT_VARIABLES[number]]: string }
>;

export const externalConfig = process.env as ExternalConfig;
