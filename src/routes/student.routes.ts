import { Router } from 'express';
import { getRepository } from 'typeorm';
import { Student } from '../models/Student';
import { validate } from 'class-validator';

const studentRouter = Router();

studentRouter.post('/', async (request, response) => {
  try {
    const studentRepository = getRepository(Student);
    const { name, key, email } = request.body;

    const student = studentRepository.create({
      name,
      key,
      email,
    });

    const errors = await validate(student);

    if (errors.length === 0) {
      const persistResponse = await studentRepository.save(request.body);
      return response.status(200).json(persistResponse);
    }

    return response.status(400).json(
      errors.map(value => ({
        field: value.property,
        error: value.constraints,
      })),
    );
  } catch ({ message }) {
    console.log(`Error message: ${message}`);
    return response.status(400).send();
  }
});

studentRouter.get('/', async (request, response) => {
  const studentRepository = getRepository(Student);
  const getStudent = await studentRepository.find();

  return response.json(getStudent);
});

export default studentRouter;
