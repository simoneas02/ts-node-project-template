import { Router } from 'express';
import { Content } from '../models/Content';
import { getRepository } from 'typeorm';

const contentRouter = Router();

contentRouter.post('/', async (request, response) => {
  try {
    const contentRepository = getRepository(Content);
    const persistResponse = await contentRepository.save(request.body);

    return response.status(200).json(persistResponse);
  } catch ({ message }) {
    console.log(`Error message: ${message}`);
    return response.status(400).send();
  }
});

contentRouter.get('/', async (request, response) => {
  const contentRepository = getRepository(Content);
  const getContent = await contentRepository.find();

  return response.json(getContent);
});

export default contentRouter;
