import { Router } from 'express';
import { getRepository } from 'typeorm';
import { Lesson } from '../models/Lesson';

const lessonRouter = Router();

lessonRouter.post('/', async (request, response) => {
  try {
    const lessonRepository = getRepository(Lesson);
    const persistResponse = await lessonRepository.save(request.body);

    return response.status(200).json(persistResponse);
  } catch ({ message }) {
    console.log(`Error message: ${message}`);
    return response.status(400).send();
  }
});

lessonRouter.get('/', async (request, response) => {
  const lessonRepository = getRepository(Lesson);
  const getLesson = await lessonRepository.find();

  return response.json(getLesson);
});

export default lessonRouter;
