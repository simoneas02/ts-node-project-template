import { Router } from 'express';
import DigitalClass from '../models/DigitalClass';
import { getConnection, getCustomRepository, getRepository } from 'typeorm';
import ClassRepository from '../repositories/DigitalClassRepository';

const digitalClassRouter = Router();

digitalClassRouter.post('/', async (request, response) => {
  try {
    const digitalClassRepository = getRepository(DigitalClass);
    const persistResponse = await digitalClassRepository.save(request.body);

    await getConnection().queryResultCache?.remove(['listDigitalClass']);

    return response.status(200).json(persistResponse);
  } catch ({ message }) {
    console.log(`Error message: ${message}`);
    return response.status(400).send();
  }
});

digitalClassRouter.get('/', async (request, response) => {
  const digitalClassRepository = getRepository(DigitalClass);
  const getClasses = await digitalClassRepository.find({
    cache: { id: 'listDigitalClass', milliseconds: 10000 },
  });

  return response.json(getClasses);
});

digitalClassRouter.get('/:name', async (request, response) => {
  const { name } = request.params;
  const digitalClassRepository = getCustomRepository(ClassRepository);

  const getByName = await digitalClassRepository.findByName(name);

  return response.json(getByName);
});

export default digitalClassRouter;
