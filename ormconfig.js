const { appConfig } = require('./dist/config/appConfig');

const databaseConfig = process.env.ENVIRONMENT ? appConfig().database : {};

module.exports = [
  {
    type: 'postgres',
    entities: ['dist/models/**/*.js'],
    migrations: ['dist/database/migrations/**/*.js'],
    subscribers: ['dist/subscribers/**/*.js'],
    cli: {
      migrationsDir: './src/database/migrations',
      entitiesDir: './src/models',
    },
    synchronize: false,
    logging: true,
    cache: { duration: 20000 },
    ...databaseConfig,
  },
];
